/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : zblog

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-01-21 00:54:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dmllog
-- ----------------------------
DROP TABLE IF EXISTS `dmllog`;
CREATE TABLE `dmllog` (
  `id` int(11) NOT NULL,
  `ip` varchar(45) DEFAULT NULL COMMENT '操作人ip 系统自动分析记录',
  `description` varchar(45) DEFAULT NULL COMMENT '业务描述',
  `duration` varchar(45) DEFAULT NULL COMMENT '操作时长 业务复杂度 自动分析记录',
  `duplicable` varchar(45) DEFAULT NULL COMMENT '操作可重复性',
  `date` datetime NOT NULL COMMENT '操作日期',
  `person` varchar(45) NOT NULL COMMENT '操作人',
  `sql` varchar(45) NOT NULL COMMENT '操作记录',
  `databackup` longtext COMMENT '数据备份',
  `action` varchar(45) DEFAULT NULL COMMENT '操作类型 CURD DDL',
  `status` varchar(45) DEFAULT NULL COMMENT '操作状态',
  `type` varchar(45) DEFAULT NULL COMMENT '操作类型 前台？ 手工？',
  `range` varchar(45) DEFAULT NULL COMMENT '操作粒度 划分方式 按数据分 按业务分',
  `attachment`
  PRIMARY KEY (`id`),
  UNIQUE KEY `sql_UNIQUE` (`sql`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='dml日志   http://blog.sina.com.cn/s/blog_4f925fc30102edg8.html';
