package OAS.Utils;

import org.junit.Test;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tony Lee on 2016/1/13.
 */
public class FileOperation extends BaseUtil {

    @Test
    public void generalTest() throws Exception {
//        System.out.println(readFile(getOrCreate("src/OAS/Resources/upload/longtextTest.txt")));
//        getOrCreate("src/OAS/test.txt");
//        deleteFile("src/OAS/test.txt");
        copyFile(new File("src/OAS/Resources/upload/test/longtextTest.txt"),
                new File("src/OAS/Resources/upload/test/test.txt"));

//        String pattern = "\\b\\w+(?=\\.)";
//        // 创建 Pattern 对象
//        Pattern r = Pattern.compile(pattern);
//        // 现在创建 matcher 对象
//        Matcher m = r.matcher("abc.txt");
//        if (m.find()) {
//            System.out.println(m.group(0));
//        }
    }

    /**
     * 基于路径的文件拷贝
     * @param src
     * @param dest
     * @return
     * @throws Exception
     */
    public static void copyFileUseURI(String src, String dest) throws Exception {
        File fileSrc = new File(src);
        writeFile(readFile(fileSrc), dest);
    }

    /**
     * 文件拷贝
     * @param src
     * @param dest
     * @throws Exception
     */
    public static void copyFile(File src, File dest) throws Exception {
        writeFile(readFile(src), dest.getPath());
    }

    /**
     * 获取和创建文件
     * @param path
     * @return
     * @throws Exception
     */
    public static File getOrCreate(String path) throws Exception {
        // 定义对于指定地址文件的一个引用(并非物理文件实体)
        File file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
            return file;
        } else {
//            System.out.println("File length: " + file.length());
            return file;
        }
    }

    /**
     * 删除文件
     * @param path
     * @throws Exception
     */
    public static void deleteFile(String path) throws Exception {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        } else {
            System.out.println("File not exists!");
        }
    }

    /**
     * 获取文件信息 包含文件名 后缀
     * @param file
     * @return
     * @throws Exception
     */
    public static String[] getFileInfo(File file) throws Exception {
        String fileName = getFileName(file.getName());
        String fileType = getFileType(file.getName());
        return new String[]{fileName, fileType};
    }

    /**
     * 截取文件名 不含后缀
     * @param name 这个参数是指file对象的文件名 包含后缀
     * @return
     */
    private static String getFileName(String name) {
        String pattern = "\\b\\w+(?=\\.)";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(name);
        if (m.find()) {
            System.out.println("fileName: " + m.group(0));
        }
        return m.group(0);
    }

    /**
     * 截取文件后缀
     * @param name
     * @return
     */
    private static String getFileType(String name) {
        String pattern = "\\.\\w+$";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(name);
        if (m.find()) {
            System.out.println("fileType: " + m.group(0).substring(1));
        }
        return m.group(0).substring(1);
    }

    /**
     * 将文本文件中的内容读入到buffer中
     *
     * @param buffer buffer
     * @param file   文件路径
     * @throws IOException 异常
     * @author cn.outofmemory
     * @date 2013-1-7
     */
    private static void readToBuffer(StringBuffer buffer, File file) throws IOException {
        InputStream is = new FileInputStream(file.getPath());
        String line; // 用来保存每行读取的内容
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        line = reader.readLine(); // 读取第一行
        while (line != null) { // 如果 line 为空说明读完了
            buffer.append(line); // 将读到的内容添加到 buffer 中
            buffer.append("\n"); // 添加换行符
            line = reader.readLine(); // 读取下一行
        }
        reader.close();
        is.close();
    }

    /**
     * 读取文本文件内容
     *
     * @param file 文件
     * @return 文本内容
     * @throws IOException 异常
     * @author cn.outofmemory
     * @date 2013-1-7
     */
    public static String readFile(File file) throws IOException {
        StringBuffer sb = new StringBuffer();
        readToBuffer(sb, file);
        return sb.toString();
    }

    /**
     * 写文件 覆盖
     *
     * @param content
     * @param path
     * @return
     * @throws IOException
     */
    public static File writeFile(String content, String path) throws IOException {
        File file = new File(path);
//            String content = "This is the content to write into file";

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile()); // 加参数true则追加不覆盖
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();

//        System.out.println("File length: " + file.length());
        return file;
    }

}
