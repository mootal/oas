package OAS.Utils;

import java.util.UUID;

/**
 * Created by Tony Lee on 2016/3/29.
 */
public class StringUtil extends BaseUtil {

    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
//        System.out.println(uuid);
        return uuid.toString();
    }

}
