package OAS.Utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Mono on 2016/3/3.
 */
public class JSONConverter extends BaseUtil {

    /**
     * 例子 http://www.studytrails.com/java/json/java-json-simple.jsp
     * <p>
     * {"title":"Free Music Archive - Genres","message":"","errors":[],"total" : "161","total_pages":81,"page":1,"limit":"2",
     * "dataSet":
     * [{"genre_id": "1","genre_parent_id":"38","genre_title":"Avant-Garde" ,"genre_handle": "Avant-Garde","genre_color":"#006666"},
     * {"genre_id":"2","genre_parent_id" :null,"genre_title":"International","genre_handle":"International","genre_color":"#CC3300"}]}
     */
    private static String indexSrc = JSONConverter.class.getResource(DY_INDEX_SRC).getPath();
    private static String indexDST = JSONConverter.class.getResource(DY_INDEX_DST).getPath();

    @Test
    public void test() throws Exception {
        // 展示压缩字符串
//        String simple = "{\"title\":\"Free Music Archive - Genres\",\"message\":\"\",\"errors\":[],\"total\" : \"161\",\"total_pages\":81,\"page\":1,\"limit\":\"2\",\n" +
//                "\"dataSet\":\n" +
//                "[{\"genre_id\": \"1\",\"genre_parent_id\":\"38\",\"genre_title\":\"Avant-Garde\" ,\"genre_handle\": \"Avant-Garde\",\"genre_color\":\"#006666\"},\n" +
//                "{\"genre_id\":\"2\",\"genre_parent_id\" :null,\"genre_title\":\"International\",\"genre_handle\":\"International\",\"genre_color\":\"#CC3300\"}]}";
//        readJson(simple);
//        System.out.println(getString(readJson(simple)));

        // 展示压缩文件
//        readJson(FileOperation.readFile(
//                FileOperation.getOrCreate("src/OAS/Resources/upload/fileArchive_v2.json")));
//        System.out.println(getString(readJson(FileOperation.readFile(
//                FileOperation.getOrCreate("src/OAS/Resources/upload/fileArchive_v2.json")))));

//        addFromBottom(buildJson(new String[]{"1", "2", "jpg", "", "Lee"}));

//        JSONObject file = readJson(FileOperation.readFile(
//                FileOperation.getOrCreate(ST_INDEX_DST)));
//        FileOperation.writeFile(getString(file), SF_INDEX_SRC);


//        // 性能测试
//        JSONObject file = readJson(FileOperation.readFile(
//                FileOperation.getOrCreate(SF_INDEX_DST)));
//        long time = System.currentTimeMillis();
//        for(int i=0;i<1000;i++){
//            // 100�� 936 844 906 928 989 | 1000�� 2127 2110 2024 2206 2115
//            FileOperation.copyFileUseURI(SF_INDEX_DST, SF_INDEX_SRC);
//
//            // 100�� 1010 952 814 773 805 | 1000�� 2896 3219 3202 2859 2847
//            FileOperation.writeFile(getString(file), SF_INDEX_SRC);
//        }
//        System.out.println(System.currentTimeMillis() - time);

        String jsonStr = FileOperation.readFile(new File(ST_INDEX_SRC));
        JSONObject jsonObj = readJson(jsonStr);
        System.out.println(jsonObj);

        findByIndex("dataSet", 1, jsonObj);
    }

    /**
     * API方法
     * 构造json对象
     *
     * @return
     * @throws Exception
     */
    public static JSONObject buildJson(String[] params) throws Exception {
        JSONObject jsonObject = new JSONObject();
        if (params == null || params.length < 3)
            return null;
        jsonObject.put("id", params[0]);
        jsonObject.put("name", params[1]);
        jsonObject.put("type", params[2]);
        jsonObject.put("date", params.length > 3 ? params[3] : new Date().toString());
        jsonObject.put("user", params.length > 4 ? params[4] : "Lee");
//        System.out.println(jsonObject.toJSONString());
        // if you want to escape characters
//        System.out.println(JSONValue.escape(jsonObject.toJSONString()));
        return jsonObject;
    }

    /**
     * API方法
     * 从底部添加一个符合规范的json对象到对象集合中
     *
     * @param newObject
     * @return
     * @throws Exception
     */
    public static JSONObject addFromBottom(String dataSetTitle, JSONObject newObject) throws Exception {

        // [准备]解析输入文件
        JSONObject json = readJson(FileOperation.readFile(
                FileOperation.getOrCreate(indexSrc)));
        JSONArray array = getDataSet(dataSetTitle, json);
        int size = array.size();

        // [业务]添加新内容
        System.out.println(json.get("total_rows"));
        json.put("total_rows", size + 1); // 更新总数
        if (newObject == null)
            return null;
        newObject.put("index", size); // 添加内容
        array.add(size, newObject);

        // [结束]输出临时文件
//        FileOperation.writeFile(getString(file), ST_INDEX_DST); // 静态调试
        String path = indexDST;
        FileOperation.writeFile(getString(json),path);

        // [结束]拷贝回原始文件
        FileOperation.copyFileUseURI(path, indexSrc);
        return json;
    }

    /**
     * API方法
     * 获取压缩后的字符串
     *
     * @param jsonObject
     * @return
     */
    public static String getString(JSONObject jsonObject) {
        return jsonObject.toJSONString();
    }

    /**
     * 从json格式的文件构造json对象
     *
     * @param jsonStr
     * @return
     * @throws Exception
     */
    public static JSONObject readJson(String jsonStr) throws ParseException {
        JSONObject jsonObject = (JSONObject) JSONValue.parseWithException(jsonStr);
//        // get the title
//        System.out.println(oasObject.get("title"));
//        // get the data
//        JSONArray genreArray = (JSONArray) oasObject.get("dataSet");
//        // test
//        System.out.println("size: " + genreArray.size());
//        // get the first genre
//        JSONObject firstGenre = (JSONObject) genreArray.get(0);
//        System.out.println(firstGenre.get("source"));
        return jsonObject;
    }

    /**
     * 从json对象解析出数据集
     *
     * @param jsonObject
     * @return
     * @throws Exception
     */
    public static JSONArray getDataSet(String dataSetTitle, JSONObject jsonObject) throws Exception {
        // get the dataSet "page_data"
        JSONArray genreArray = (JSONArray) jsonObject.get(dataSetTitle);
        return genreArray;
    }


//    public static JSONObject addFromTop(JSONObject newObject) {
//
//    }
//
//    public static JSONObject replace(int index, JSONObject newObject) {
//
//    }
//
//    public static JSONObject removeFromTop() {
//
//    }
//
//    public static JSONObject removeFromBottom() {
//
//    }
//
//    public static HashMap findById(String id) {
//
//    }

    /**
     * 从json序列依据索引值找出某个元素
     *
     * @param index
     * @param jsonObject
     * @return
     * @throws Exception
     */
    public static HashMap findByIndex(String dataSetTitle, int index, JSONObject jsonObject) throws Exception {
        JSONArray jsonArray = getDataSet(dataSetTitle, jsonObject);
        HashMap item = (HashMap)jsonArray.get(index);
        return item;
    }


}
