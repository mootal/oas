package OAS.Utils;

import OAS.Infrastructure.Configuration;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

/**
 * Created by Tony Lee on 2016/3/25.
 */
public class BaseUtil implements Configuration {

    public static void main(String[] args) throws Exception {
        String indexes = "[3,5]";
        JSONArray indexJson = (JSONArray) JSONValue.parseWithException(indexes);
        for(Object index : indexJson) {
            System.out.println(index instanceof Long);
            long l = (Long)index;
            int i = (int)l;
            System.out.println(i);
        }
    }

}
