/**
 * Copyright (c) 2014 Alibaba Cloud Computing
 */
package OAS.Utils;

import java.util.ArrayList;

/**
 *
 */
public class CollectionUtils extends BaseUtil {

    public static <T> String arrayToString(T[] t) {
        String result = "";
        if (t != null)
            for (T element : t) {
                if(element != null)
                    result += element.toString() + ",";
            }
        return result.substring(0, result.length() - 1);
    }

    public static void main(String[] args) {
        Integer[] fIds = new Integer[2];
        ArrayList<Integer> failedIds = new ArrayList();
        for (int i = 1; i <= 2; i++) {
            failedIds.add(i);
        }
        System.out.println(arrayToString(failedIds.toArray(fIds)));
    }
}
