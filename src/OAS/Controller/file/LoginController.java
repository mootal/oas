package OAS.Controller.file;

/**
 * Created by Tony Lee on 2016/4/13.
 */

import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@SuppressWarnings({"serial"})
@WebServlet(name = "loginController", urlPatterns = {"/login.do"})
public class LoginController extends HttpServlet implements BaseController {

    public LoginController() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        /*** ▼ *** ▼ *** ▼ *** 获取前台数据 *** ▼ *** ▼ *** ▼ ***/


        /*** ▼ *** ▼ *** ▼ *** 返回数据类型 *** ▼ *** ▼ *** ▼ ***/
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=GBK");
        JSONObject obj = new JSONObject();

        /*** ▼ *** ▼ *** ▼ *** 业务基本操作 *** ▼ *** ▼ *** ▼ ***/
//        System.out.println(request.getServletContext().getRealPath(""));
//        System.out.println(request.getServletContext().getRealPath("/"));
//        // E:\Workspaces\OAS\oas\out\artifacts\OAS\
//        File file = new File(request.getServletContext().getRealPath("/") + "WEB-INF/classes/", "b.properties");
//        if (file.exists()) {
//            file.delete();
//        }
//        file.createNewFile();
//
//        Properties prop = new Properties();
//        FileOutputStream oFile = new FileOutputStream(file); // true参数表示追加打开
//        prop.put("VaultName", vaultName); //"1"
//        prop.put("AccessKeyId", accessKeyId); //"2"
//        prop.put("AccessKeySecret", accessKeySecret); //"3"
//        prop.store(oFile, "The New properties file");
//        oFile.flush();
//        oFile.close();

        OAS.Console.login(request, response);
        obj.put("status", DONE);
        response.getWriter().print(obj.toJSONString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        System.out.println("GET");
    }

    /**
     * 设置cookie
     * @param response
     * @param name  cookie名字
     * @param value cookie值
     * @param maxAge cookie生命周期  以秒为单位
     */
    public static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        if (maxAge > 0) cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }

    /**
     * 根据名字获取cookie
     * @param request
     * @param name cookie名字
     * @return
     */
    public static Cookie getCookieByName(HttpServletRequest request, String name) {
        Map<String, Cookie> cookieMap = ReadCookieMap(request);
        if (cookieMap.containsKey(name)) {
            Cookie cookie = (Cookie) cookieMap.get(name);
            return cookie;
        } else {
            throw new RuntimeException("get cookie failed");
        }
    }

    /**
     * 将cookie封装到Map里面
     * @param request
     * @return
     */
    private static Map<String, Cookie> ReadCookieMap(HttpServletRequest request) {
        Map<String, Cookie> cookieMap = new HashMap<String, Cookie>();
        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                cookieMap.put(cookie.getName(), cookie);
            }
        }
        return cookieMap;
    }

    public static void main(String[] args) throws ServletException,
            IOException {
        Properties prop = new Properties();
//        prop.load(new FileInputStream( "b.properties" ));// 修改已有

        FileOutputStream oFile =
                new FileOutputStream("b.properties"); // true参数表示追加打开
        prop.put("VaultName", "1");
        prop.put("AccessKeyId", "2");
        prop.put("AccessKeySecret", "3");
        prop.store(oFile, "The New properties file");
        oFile.close();
    }

}