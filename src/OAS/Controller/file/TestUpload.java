package OAS.Controller.file;

import OAS.Console;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * BUG
 * 1 ??????????
 * 2 ????????????
 * 3 ????????????
 * 4 ???��??????
 * 5 js json????
 * 6 java ???????
 * 7 script��?? $(function
 * 8 json��ҳ����
 * 9 ����
 * */
@WebServlet("/FileUploadServlet")
public class TestUpload extends HttpServlet implements BaseController {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println("doGet");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*** ?? *** ?? *** ?? *** ????????? *** ?? *** ?? *** ?? ***/
        String action = request.getParameter("action");

        /*** ?? *** ?? *** ?? *** ???????????? *** ?? *** ?? *** ?? ***/
        response.setContentType("application/json;charset=UTF-8");
//        response.setContentType("text/html;charset=UTF-8");
        JSONObject obj = new JSONObject();

        /*** ?? *** ?? *** ?? *** ?????????? *** ?? *** ?? *** ?? ***/
        //????????fileName
        String fileName = null;

        //?��?????  http://commons.apache.org/proper/commons-fileupload/using.html
        // Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            // Create a factory for disk-based file items
            DiskFileItemFactory factory = new DiskFileItemFactory();

            // Configure a repository (to ensure a secure temp location is used)
            ServletContext servletContext = this.getServletConfig().getServletContext();
            File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
            factory.setRepository(repository);

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            // Parse the request
            try {
                List<FileItem> items = upload.parseRequest(request);
                for (FileItem item : items) {
                    //????????
                    String type = item.getContentType();
                    if (type == null) {
//                  System.out.println(item.getString(item.getFieldName()));
                        continue;
                    }

                    //???????
                    fileName = item.getName();

                    //??????????��??
                    String realPath = request.getServletContext().getRealPath("/html/image");
                    File dir = new File(realPath);
                    File file = new File(dir, fileName);

                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();

                    //????
                    item.write(file);

                    /*** ?? *** ?? *** ?? *** ???????? *** ?? *** ?? *** ?? ***/
                    if (action != null || 1 == 1) {
                        try {
                            obj.put("storage", realPath);

                            String[] oasInfo = Console.addToRemote(request, file, "", "");
                            oasInfo[1] = fileName;
                            oasInfo[2] = ""; // type
                            obj.put("oasId", oasInfo[0]);
                            Console.updateIndex("dataSet", oasInfo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*** ?? *** ?? *** ?? *** ?????? *** ?? *** ?? *** ?? ***/
        obj.put("fileName", fileName);
//        response.getWriter().print(obj.toJSONString());
        response.getWriter().print("{\"name\":\"json data .....\"}");
    }

    public static void main(String[] args) {
        System.out.println(TestUpload.class.getResource("../../Resources/upload/fileArchive_v2.json").getPath());
        File f = new File(TestUpload.class.getResource("abc.txt").getPath());
        System.out.println(f.exists());

        String[] a = new String[]{"a", ""};
        a[1] = "1";
        System.out.println(a.length);
    }
}