package OAS.Controller.file;

/**
 * Created by Tony Lee on 2016/3/29.
 */

import OAS.Utils.StringUtil;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;

/** basic fileupload class DEBUGING */
@SuppressWarnings({"serial"})
public class SimpleUpload extends HttpServlet implements BaseController {
    //???????????????M??
    public static final int MAX_SIZE = 1024 * 1024 * 100;
    //???????????????
    private static final String DIR = "UploadFiles/";
    //???????WEB??????·??
    private String FILE_DIR;

    private int file_Size = 0;
    private String file_Path = "";
    private HashMap<String, String> hm = new HashMap<>();

    public SimpleUpload() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            Iterator items;
            try {
                items = upload.parseRequest(request).iterator();
                while (items.hasNext()) {
                    FileItem item = (FileItem) items.next();
                    if (!item.isFormField()) {
                        String name = item.getName();
                        String fileName = name.substring(name.lastIndexOf('\\') + 1, name.length());
                        String path = this.getServletContext().getRealPath("/") + File.separatorChar + fileName;
                        File uploadedFile = new File(path);
                        item.write(uploadedFile);
                        response.setContentType("text/html");
                        response.setCharacterEncoding("gb2312");
                        PrintWriter out = response.getWriter();
                        out.print("<font size='2'>upload the file is:" + name + "<br>");
                        out.print("save the path is:" + path + "</font>");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String path = this.getServletConfig().getServletContext().getRealPath("/");
        FILE_DIR = path.replaceAll("\\\\", "/") + DIR;
        String result = upload(request);
        PrintWriter out = null;
        try {
            response.setContentType("text/html;charset=UTF-8");
            out = response.getWriter();
            out.print(result);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }

    //??????
    private String upload(HttpServletRequest req) {
        String tmpString;
        String result = "";
        DataInputStream dis = null;
        try {
            dis = new DataInputStream(req.getInputStream());
            String content = req.getContentType();
            if (content != null && content.indexOf("multipart/form-data") != -1) {
                int reqSize = req.getContentLength();
                byte[] data = new byte[reqSize];

                int bytesRead;
                int totalBytesRead = 0;
                int sizeCheck;
                while (totalBytesRead < reqSize) {
                    // check for maximum file size violation
                    sizeCheck = totalBytesRead + dis.available();
                    if (sizeCheck > MAX_SIZE)
                        result = "???????????...";
                    bytesRead = dis.read(data, totalBytesRead, reqSize);
                    totalBytesRead += bytesRead;
                }
                tmpString = new String(data);
                hm = parseAnotherParam(tmpString);
                int postion = arrayIndexOf(data, "\r\n".getBytes());
                byte[] split_arr = new byte[postion];
                System.arraycopy(data, 0, split_arr, 0, postion);

                postion = arrayIndexOf(data, "filename=\"".getBytes());
                byte[] dataTmp = new byte[data.length - postion];
                System.arraycopy(data, postion, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();
                String filePath = null;
                postion = arrayIndexOf(data, "Content-Type:".getBytes()) - 2;
                dataTmp = null;
                dataTmp = new byte[postion];
                System.arraycopy(data, 0, dataTmp, 0, dataTmp.length);
                filePath = new String(dataTmp);
                if (filePath == null && filePath.equals("")) return "";
                // ????contentType ?????
                postion = arrayIndexOf(data, "Content-Type:".getBytes());
                dataTmp = null;
                dataTmp = new byte[data.length - postion];
                System.arraycopy(data, postion, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();
                postion = arrayIndexOf(data, "\n".getBytes()) + 1;
                dataTmp = null;
                dataTmp = new byte[data.length - postion];
                System.arraycopy(data, postion, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();

                // ?????????? ???????????????
                postion = arrayIndexOf(data, split_arr);
                split_arr = null;
                dataTmp = null;
                dataTmp = new byte[postion - 2];
                System.arraycopy(data, 2, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();

                postion = arrayLastIndexOf(data, "\n".getBytes()) - 1;
                dataTmp = null;
                dataTmp = new byte[postion];
                System.arraycopy(data, 0, dataTmp, 0, dataTmp.length);

                data = null;
                String fileName = getFileName(filePath);
                String UUID = StringUtil.getUUID();
                if (null != fileName) {
                    File file = new File(FILE_DIR);
                    boolean existsFlag = false;
                    if (!(existsFlag = file.exists())) {
                        existsFlag = file.mkdirs();
                    }
                    //???????????????Ψ???????????????????????????
                    int lastIndex = fileName.lastIndexOf(".");
                    if (lastIndex == -1) {
                        fileName = UUID;
                    } else {
                        fileName = UUID + fileName.substring(lastIndex, fileName.length());
                    }
                    if (writeFile(dataTmp, FILE_DIR + fileName) && existsFlag) {
                        this.file_Size = dataTmp.length;
                        this.file_Path = FILE_DIR + fileName;
                        result = "?????????." + DIR + fileName;
                    } else {
                        result = "?????????";
                    }
                } else {
                    result = "????????";
                }
                dataTmp = null;
            } else {
                result = "content ????? multipart/form-data";
            }
        } catch (UnsupportedEncodingException ex4) {
            result = "UnsupportedEncodingException????";
        } catch (NullPointerException e) {
            result = "NullPointerException????";
        } catch (IOException ex1) {
            result = "IOException ???? ";
        } catch (Exception ex1) {
            result = "Exception ???? " + ex1.getMessage();
        } finally {
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    //??????????
    private boolean writeFile(byte[] data, String path) {
        File f;
        FileOutputStream fos = null;
        try {
            f = new File(path);
            f.createNewFile();
            fos = new FileOutputStream(f);
            fos.write(data, 0, data.length);
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    //????????
    private String getFileName(String arg) {
        String path;
        if (arg.equals("\"\"")) {
            return null;
        }

        if (arg.indexOf("\"") > -1)
            path = arg.substring(arg.indexOf("\"") + 1, arg.lastIndexOf("\""));
        else
            path = arg;
        path = path.substring(path.lastIndexOf("\\") + 1);
        return path;
    }


    //?ж?????byte????????????
    private boolean arrayEquals(byte[] src, byte[] value) {
        if (src == null || value == null)
            return false;
        if (src.length != value.length)
            return false;

        for (int i = 0; i < src.length; i++) {
            if (src[i] != value[i])
                return false;
        }
        return true;
    }


    //???value??????src?е?λ??, ???????
    private int arrayIndexOf(byte[] src, byte[] value) {
        if (src == null || value == null)
            return -1;
        if (src.length < value.length)
            return -1;

        int postion = -1;

        for (int i = 0; i < src.length - value.length; i++) {
            postion = i;
            byte[] tmp = new byte[value.length];
            System.arraycopy(src, i, tmp, 0, tmp.length);
            if (arrayEquals(tmp, value)) {
                tmp = null;
                return postion;
            } else {
                postion = -1;
                tmp = null;
            }
        }

        return postion;
    }


    //???value??????src?е?λ??
    private int arrayLastIndexOf(byte[] src, byte[] value) {
        if (src == null || value == null)
            return -1;
        if (src.length < value.length)
            return -1;

        int postion = -1;

        for (int i = src.length - value.length; i > -1; i--) {
            postion = i;

            byte[] tmp = new byte[value.length];
            System.arraycopy(src, i, tmp, 0, tmp.length);
            if (arrayEquals(tmp, value)) {
                tmp = null;
                return postion;
            } else {
                postion = -1;
                tmp = null;
            }
        }
        return postion;
    }

    private HashMap<String, String> parseAnotherParam(String str) {
        HashMap<String, String> hm = new HashMap<>();
        String key = "";
        String value = "";
        int startindex = 0;
        int endindex = 0;

        startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                + "Content-Disposition: form-data; name=\"".length();
        endindex = str.indexOf("\"\r\n\r\n");

        while (startindex > -1 && endindex > -1) {
            key = str.substring(startindex, endindex);

            if (!str.substring(endindex, endindex + 5).equals("\"\r\n\r\n")) {//??????value?????
                str = str.substring(endindex);
                startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                        + "Content-Disposition: form-data; name=\"".length();
                endindex = str.indexOf("\"\r\n\r\n");
                continue;
            }
            if (key.indexOf("\";") > -1) {//?????????????????????
                str = str.substring(str.indexOf("\";") + 2);
                startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                        + "Content-Disposition: form-data; name=\"".length();
                endindex = str.indexOf("\"\r\n\r\n");

                continue;
            } else
                str = str.substring(endindex + 5);

            value = str.substring(0, str.indexOf("\r\n"));
            str = str.substring(str.indexOf("\r\n") + 2);
            hm.put(key, value);

            startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                    + "Content-Disposition: form-data; name=\"".length();
            endindex = str.indexOf("\"\r\n\r\n");

        }
        return hm;
    }
}