package OAS.Controller.file;

import OAS.Console;
import OAS.Infrastructure.Configuration;
import OAS.Utils.CollectionUtils;
import OAS.Utils.FileOperation;
import OAS.Utils.JSONConverter;
import com.aliyun.oas.model.exception.OASClientException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Tony Lee on 2016/3/30.
 */
@MultipartConfig
@WebServlet(name = "fileController", urlPatterns = {"/server.do"})
public class FileController extends HttpServlet implements BaseController {
    /** */
    private static final long serialVersionUID = 1L;

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
//        processRequest(request, response);

        /*** ▼ *** ▼ *** ▼ *** 获取前台数据 *** ▼ *** ▼ *** ▼ ***/
        String action = request.getParameter("action");
        System.out.println("action: " + action);

        /*** ▼ *** ▼ *** ▼ *** 返回数据类型 *** ▼ *** ▼ *** ▼ ***/
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=GBK");
        JSONObject obj = new JSONObject();

        /*** ▼ *** ▼ *** ▼ *** 业务基本操作 *** ▼ *** ▼ *** ▼ ***/
        // 查询

        // 批量下载
        if (action.equals(DOWNLOAD)) {
            try {
                String path = Configuration.class.getResource(DY_INDEX_SRC).getPath();
                String jsonStr = FileOperation.readFile(new File(path));
                JSONObject jsonObj = JSONConverter.readJson(jsonStr);
                String indexes = request.getParameter("index");
                JSONArray indexJson = (JSONArray) JSONValue.parseWithException(indexes);
//                while (indexJson.iterator().hasNext()) {
//                    indexJson.iterator().next();
//                    int index = Integer.valueOf(indexJson.iterator().next().toString());
//                    try {
//                        Console.dlFromRemote((String) JSONConverter.
//                                    findByIndex("page_data", index, jsonObj).get("id"),
//                            DY_DL_Folder);
//                    } catch (OASClientException e) {
//                        e.printStackTrace();
//                        obj.put("index", index);
//                    }
//                }
                Integer[] eIds = new Integer[indexJson.size()];
                String[] fIds = new String[indexJson.size()];
                ArrayList<Integer> failedIds = new ArrayList(); // 失败索引
                ArrayList<String> fileIds = new ArrayList(); // 成功id
                for (Object index : indexJson) {
                    long l = (Long) index;
                    int i = (int) l;
                    try {
                        String id = (String) JSONConverter.
                                findByIndex("page_data", i, jsonObj).get("id");
                        fileIds.add(id);
                        // TODO 修改路径
                        Console.dlFromRemote(request, id, SF_DL_Folder
                                /*Configuration.class.getResource(DY_DL_Folder).getPath()*/);
                    } catch (OASClientException e) {
                        e.printStackTrace();
                        failedIds.add(i);
                    }
                }
                if(failedIds.size() > 0)
                    obj.put("failNo", CollectionUtils.arrayToString(failedIds.toArray(eIds)));
                obj.put("fileIds", CollectionUtils.arrayToString(fileIds.toArray(fIds)));
                obj.put("status", DONE);
            } catch (ParseException e) {
                e.printStackTrace();
                obj.put("status", ALWAYS);
            } catch (Exception e) {
                e.printStackTrace();
                obj.put("status", ALWAYS);
            }
        }

        /*** ▼ *** ▼ *** ▼ *** 返回结果 *** ▼ *** ▼ *** ▼ ***/
        response.getWriter().print(obj.toJSONString());
    }

    /**
     * 使用Servlet 3.0 MultipartConfig处理 必须使用@MultipartConfig进行标注
     * http://haohaoxuexi.iteye.com/blog/2013691
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        /*** ▼ *** ▼ *** ▼ *** 获取前台数据 *** ▼ *** ▼ *** ▼ ***/
        String action = request.getParameter("action");
        System.out.println("action: " + action);

        /*** ▼ *** ▼ *** ▼ *** 返回数据类型 *** ▼ *** ▼ *** ▼ ***/
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=GBK");
        JSONObject obj = new JSONObject();

        /*** ▼ *** ▼ *** ▼ *** 业务基本操作 *** ▼ *** ▼ *** ▼ ***/
        if (action == null) { // 列表
            try {
                String pageNum = request.getParameter("page_num");
                String rowsPerPage = request.getParameter("rows_per_page");
                String path = Configuration.class.getResource(DY_INDEX_SRC).getPath();
                String jsonStr = FileOperation.readFile(new File(path));
                JSONObject jsonObj = JSONConverter.readJson(jsonStr);
                response.getWriter().print(jsonObj.toJSONString());
                return;
            } catch (Exception e) {
                e.printStackTrace();
                response.getWriter().print(FAIL);
            }
        }

        if (action.contains(UPLOAD)) { // 上传
            // 需要返回的fileName
            String fileName = null;

            Part part = request.getPart("upload");
            if (part != null) {
                //格式如：form-data; name="upload"; filename="YNote.exe"
                String disposition = part.getHeader("content-disposition");
                System.out.println(disposition);
                fileName = disposition.substring(disposition.lastIndexOf("=") + 2, disposition.length() - 1);
                String fileType = part.getContentType();
                long fileSize = part.getSize();
                System.out.println("fileName: " + fileName);
                System.out.println("fileType: " + fileType);
                System.out.println("fileSize: " + fileSize);

                //设置保存文件路径
                String uploadPath = request.getServletContext().getRealPath("/html/upload");
                System.out.println("uploadPath: " + uploadPath);
                File dir = new File(uploadPath);
                File file = new File(dir, fileName);
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();

                //保存
                part.write(uploadPath + File.separator + fileName);
                obj.put("fileType", fileType);
                obj.put("storage", uploadPath);

                /*** ▼ *** ▼ *** ▼ *** 定制需求 *** ▼ *** ▼ *** ▼ ***/
                if (action != null && action.contains(OASUPLOAD)) {
                    try {
                        String[] oasInfo = Console.addToRemote(request, file, "", "");
                        oasInfo[1] = fileName;
                        oasInfo[2] = fileType; // type
                        obj.put("oasId", oasInfo[0]);
                        // 更新索引
                        Console.updateIndex("page_data", oasInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                        obj.put("status", FAIL);
                    }
                }
            }
            obj.put("fileName", fileName); // 返回fileName
        }

        /*** ▼ *** ▼ *** ▼ *** 返回结果 *** ▼ *** ▼ *** ▼ ***/
        response.getWriter().print(obj.toJSONString());
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=GBK");
        String action = request.getParameter("action");
        if ("uploadFile".equals(action)) {
            this.uploadFile(request, response);  //??????
        }
    }

    public void uploadFile(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=GBK");
        request.setCharacterEncoding("GBK");
        HttpSession session = request.getSession();
        session.setAttribute("progressBar", 0);      //???????????????Session????
        String error = "";
        int maxSize = 10 * 1024 * 1024;        //????????????С??????
        DiskFileItemFactory factory = new DiskFileItemFactory();        //????????????????????????????
        ServletFileUpload upload = new ServletFileUpload(factory);  //????????????????????
        try {
            List items = upload.parseRequest(request);// ???????????
            Iterator itr = items.iterator();// ??????
            while (itr.hasNext()) {
                FileItem item = (FileItem) itr.next();  //???FileItem????
                if (!item.isFormField()) {// ?ж??????????
                    if (item.getName() != null && !item.getName().equals("")) {// ?ж????????????
                        long upFileSize = item.getSize();     //?????????С
                        String fileName = item.getName();     //????????
                        //System.out.println("?????????С:" + item.getSize());
                        if (upFileSize > maxSize) {
                            error = "?????????????????????50M?????";
                            break;
                        }
                        // ???????????????????????
                        File tempFile = new File(fileName);// ???????????
                        String uploadPath = this.getServletContext().getRealPath("/upload");
                        File file = new File(uploadPath, tempFile.getName());   // ???????????????????·??
                        InputStream is = item.getInputStream();
                        int buffer = 1024;     //???建???????С
                        int length = 0;
                        byte[] b = new byte[buffer];
                        double percent = 0;
                        FileOutputStream fos = new FileOutputStream(file);
                        while ((length = is.read(b)) != -1) {
                            percent += length / (double) upFileSize * 100D;        //???????????????
                            fos.write(b, 0, length);                    //??????????д?????????
                            session.setAttribute("progressBar", Math.round(percent));    //???????????浽Session??
                        }
                        fos.close();
                        Thread.sleep(1000);     //???????1??
                    } else {
                        error = "??????????????";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = "?????????????" + e.getMessage();
        }
        if (!"".equals(error)) {
            request.setAttribute("error", error);
            request.getRequestDispatcher("error.jsp").forward(request, response);
        } else {
            request.setAttribute("result", "???????????");
//            request.getRequestDispatcher("upFile_deal.jsp").forward(request, response);
        }
    }

}
