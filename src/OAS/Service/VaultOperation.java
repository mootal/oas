/**
 * Copyright (c) 2014 Alibaba Cloud Computing
 */
package OAS.Service;

import OAS.Controller.file.LoginController;
import OAS.Infrastructure.Configuration;
import com.aliyun.oas.OASFactory;
import com.aliyun.oas.core.AliyunOASClient;
import com.aliyun.oas.ease.ArchiveManager;
import com.aliyun.oas.ease.monitor.JobMonitor;
import com.aliyun.oas.model.common.ClientConfiguration;
import com.aliyun.oas.model.common.ServiceCredentials;
import com.aliyun.oas.model.common.ServiceHost;
import com.aliyun.oas.model.descriptor.JobDescriptor;
import com.aliyun.oas.model.exception.OASClientException;
import com.aliyun.oas.model.exception.OASServerException;
import com.aliyun.oas.model.request.CreateVaultRequest;
import com.aliyun.oas.model.request.ListJobsRequest;
import com.aliyun.oas.model.request.ListVaultsRequest;
import com.aliyun.oas.model.result.CreateVaultResult;
import com.aliyun.oas.model.result.ListJobsResult;
import com.aliyun.oas.model.result.ListVaultsResult;
import com.aliyun.oas.model.result.UploadResult;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 创建vault demo程序
 * 每个用户最多创建10个vault
 *
 * @author jialan@alibaba-inc.com
 * @version $Id: VaultOperation.java, v 0.1 2015-5-12 上午11:18:20 jialan Exp $
 */
public class VaultOperation extends BaseService {

    //Create the logger
    private static final Logger logger = LoggerFactory.getLogger(VaultOperation.class);
//    //初始化认证信息
//    private static ServiceCredentials credentials;
//    //服务地址
//    private static ServiceHost serviceHost;
//    //客户端配置
//    private static ClientConfiguration clientConfiguration;
//    // 方法1
//    private static AliyunOASClient aliyunOASClient;
//    // oasID
//    private String AccessKeyId;
//    // oas密码
//    private String AccessKeySecret;
//    // oas库名
//    private String VaultName;

    static {
//
    }

    public static void main(String[] args) {
        System.out.println(Configuration.class.getResource("/").getPath());
        // /E:/Workspaces/OAS/oas/out/production/HFS/
        File file = new File(Configuration.class.getResource("/b.properties").getPath());
        if (file.exists()) {
            file.delete();
        }
        //        认证方法2
        //        AliyunOASClient aliyunOASClient2 = OASFactory.aliyunOASClientFactory(credentials,
        //            "http://cn-hangzhou.oas.aliyuncs.com");

        //        AliyunOASClient aliyunOASClient2 = OASFactory.getEmptyAliyunOASClient().withLogger();
        //        aliyunOASClient2
        //            .withServiceCredentials(new ServiceCredentials(yourAccessId, yourAccessKey));
        //
        //        aliyunOASClient2.withHost("http://cn-hangzhou.oas.aliyuncs.com/");

        // 或者像下面这样，通过url创建AliyunOASClient对象
        //        AliyunOASClient aliyunOASClient = OASFactory.aliyunOASClientFactory(credentials,
        //            "http://cn-hangzhou.oas.aliyuncs.com").withLogger();

        // vaultName必须满足已下2个条件：
        // 1. 总长度在3~63之间（包括3和63）；
        // vaultName必须满足已下2个条件：
        // 1. 总长度在3~63之间（包括3和63）；
        // 2. 只允许包含以下字符：
        //         0-9(数字),
        //         a-z(小写英文字母),
        //         -(短横线),
        //         _(下划线)
        // 其中 短横线 和 下划线 不能作为vaultName的开头和结尾；
    }

    /**
     * 登录
     */
    public /*static*/ void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        Properties prop = new Properties();
//        try {
//            prop.load(new FileInputStream(
////                    "b.properties"
//                    Configuration.class.getResource("/").getPath() + "b.properties"
//            ));
//            AccessKeyId = prop.getProperty("AccessKeyId");
//            AccessKeySecret = prop.getProperty("AccessKeySecret");
//            VaultName = prop.getProperty("VaultName");
//
//            System.out.println(Configuration.class.getResource("/").getPath());
//            // /E:/Workspaces/OAS/oas/out/artifacts/OAS/WEB-INF/classes/
//            File file = new File( Configuration.class.getResource("/").getPath() + "b.properties");
//            if (file.exists()) {
//                file.delete();
//            }
//        } catch(IOException e) {
//            e.printStackTrace();
//        }

        String AccessKeyId = request.getParameter("accessKeyId");
        String AccessKeySecret = request.getParameter("accessKeySecret");
        String VaultName = request.getParameter("vaultName");

        //初始化认证信息
        ServiceCredentials credentials = new ServiceCredentials(AccessKeyId, AccessKeySecret);
        //服务地址
        ServiceHost serviceHost = new ServiceHost(PROTOCOL_HTTP + RegionName_HangZhou + PUBLIC_DOMAIN, PORT_80);
        //客户端配置
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        //认证方法1
        AliyunOASClient aliyunOASClient = OASFactory
                .aliyunOASClientFactory(serviceHost, credentials, clientConfiguration).withLogger();

        // 设置用户cookie和session信息
        LoginController.addCookie(response, "accessKeyId", AccessKeyId, cookieExpire);
        LoginController.addCookie(response, "accessKeySecret", AccessKeySecret, cookieExpire);
        LoginController.addCookie(response, "vaultName", VaultName, cookieExpire);
        request.getSession().setAttribute("aliyunOASClient", aliyunOASClient);
        request.getSession().setMaxInactiveInterval(sessionExpire);
    }

    /**
     * 建库
     */
    public /*static*/ void createVault(HttpServletRequest request) {
        String VaultName = LoginController.getCookieByName(request, "vaultName").getValue();

        // 发送创建vault请求
        // 创建Vault的名称，用CreateVaultRequest来指定
        CreateVaultRequest createRequest = new CreateVaultRequest().withVaultName(VaultName);

        // 发起创建Vault的请求
        // 如果有同名的vault存在，OAS会返回已有的vault的vaultId，而不会执行创建动作
        try {
            AliyunOASClient aliyunOASClient = (AliyunOASClient)request.getSession().getAttribute("aliyunOASClient");
            CreateVaultResult result = aliyunOASClient.createVault(createRequest);
            logger.info("Vault created vaultId={}", result.getVaultId());
            logger.info("Vault created vaultLocation={}", result.getLocation());
        } catch (OASClientException e) {
            logger.error("OASClientException Occured:", e);
        } catch (OASServerException e) {
            logger.error("OASServerException Occured:", e);
        }
    }

    /**
     * 普通文件同步上传 建议查询采用同步方式 上传下载采用异步方式
     */
    public /*static*/ UploadResult archiveManager(HttpServletRequest request, File file) {
        String VaultName = LoginController.getCookieByName(request, "vaultName").getValue();
        AliyunOASClient aliyunOASClient = (AliyunOASClient)request.getSession().getAttribute("aliyunOASClient");

        ArchiveManager archiveManager = OASFactory.archiveManagerFactory(aliyunOASClient).withNumConcurrence(5).withMaxRetryTimePerRequest(3);
        UploadResult uploadResult = null;
        if (file == null || !file.exists())
            file = new File(Test_file);
        if (file.exists())
            try {
                uploadResult = archiveManager.upload(VaultName, file, "first test file");
                logger.info("File {} uploaded complete. ArchiveId={},md5={},treeEtag={}",
                        file.getAbsolutePath(),
                        uploadResult.getArchiveId(),
                        uploadResult.getContentEtag(),
                        uploadResult.getTreeEtag());

            } catch (OASClientException e) {
                logger.error("OASClientException Occured:", e);
            } catch (OASServerException e) {
                logger.error("OASServerException Occured:", e);
            }
        return uploadResult;
    }

    /**
     * 大文件上传
     */
    public /*static*/ UploadResult multipartUpload(HttpServletRequest request, File file) {
        String VaultName = LoginController.getCookieByName(request, "vaultName").getValue();
        AliyunOASClient aliyunOASClient = (AliyunOASClient)request.getSession().getAttribute("aliyunOASClient");

        ArchiveManager archiveManager = OASFactory.archiveManagerFactory(aliyunOASClient).withNumConcurrence(5).withMaxRetryTimePerRequest(3);
        UploadResult uploadResult = null;
        if (file == null || !file.exists())
            file = new File(Test_Bigfile);
        if (file.exists()) {
            //获得uploadId
            //文件大小必须大于100MB，否则会抛异常提示用普通上传接口进行上传
            String uploadId = archiveManager.initiateMultipartUpload(VaultName, file, "Hello OAS!");
            //使用已有的uploadId进行再次上传时，支持该任务的续传。
            //String uploadId = "[yourUploadId]";
            System.out.println("Get uploadId=" + uploadId);
            uploadResult = archiveManager.uploadWithUploadId(VaultName, file, uploadId);
            System.out.println("Archive ID=" + uploadResult.getArchiveId());
        }
        return uploadResult;
    }

    /**
     * 下载 <br/>
     * 1 用户 通过接口提交相应类型的Job；<br/>
     * 2 OAS 接收到Job并安排其执行；<br/>
     * 3 用户 将已完成Job的输出内容下载到本地。
     */
    public /*static*/ void download(HttpServletRequest request, String archiveId, String dest) {
        AliyunOASClient aliyunOASClient = (AliyunOASClient)request.getSession().getAttribute("aliyunOASClient");
        String VaultName = LoginController.getCookieByName(request, "vaultName").getValue();

        ArchiveManager archiveManager = OASFactory.archiveManagerFactory(aliyunOASClient).withNumConcurrence(5).withMaxRetryTimePerRequest(3);
        // 提交提档任务
        JobMonitor jobMonitor = archiveManager.downloadAsync(VaultName, archiveId);
        // 执行Inventory
        archiveManager.downloadInventoryAsync(VaultName);
        // 下载Job输出
        archiveManager.downloadJobOutput(VaultName, jobMonitor.getJobId(),
                new File(dest + archiveId));
    }

    /**
     * 历史记录列表
     */
    public /*static*/ void lookHistory(HttpServletRequest request, String vaultId, int jobsLimit) {
        AliyunOASClient aliyunOASClient = (AliyunOASClient)request.getSession().getAttribute("aliyunOASClient");

        if (vaultId == null) { // default
            // 1 获取库信息
            ListVaultsRequest vaultsRequest = new ListVaultsRequest();
            vaultsRequest.withLimit(10);
            vaultsRequest.withMarker("");
            ListVaultsResult list =
                    aliyunOASClient.listVaults(vaultsRequest);
            System.out.println(
                    "MARKER:" + list.getMarker());
            vaultId = list.getVaultList().iterator().next().getVaultId();
            System.out.println(
                    "VAULT ID: " + vaultId);
            System.out.println();
        }

        // 2 通过库信息获取job历史信息
        ListJobsRequest jobsRequest = new ListJobsRequest();
        // 关键
        jobsRequest.withLimit(jobsLimit);
        jobsRequest.withMarker("");
        jobsRequest.setVaultId(vaultId);
        ListJobsResult jobs = aliyunOASClient.listJobs(jobsRequest);
        System.out.println(
                "MARKER:" + jobs.getMarker());
        List<JobDescriptor> jobList = jobs.getJobList();
        System.out.println("JOBS SIZE: " + jobList.size());
        System.out.println();

        for (JobDescriptor job : jobList) {
            System.out.println("@@@CreationDate: " + job.getCreationDate());
            System.out.println("JobStatus: " + job.getJobStatus());
            System.out.println("ArchiveId: " + job.getArchiveId());
            System.out.println("ArchiveSize: " + job.getArchiveSize());
        }
    }

    @Test
    public void test() {

//        ArchiveManager archiveManager = OASFactory.archiveManagerFactory(aliyunOASClient).withNumConcurrence(5).withMaxRetryTimePerRequest(3);
//        archiveManager.downloadInventoryAsync(VaultName);

//        createVault();
//        archiveManager();
//        multipartUpload();
//        lookHistory(null, 1000);
//        download(TechBook_fileId, Test_dest);
    }
}
