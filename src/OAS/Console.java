package OAS;

import OAS.Infrastructure.Configuration;
import OAS.Service.VaultOperation;
import OAS.Utils.FileOperation;
import OAS.Utils.JSONConverter;
import com.aliyun.oas.model.result.UploadResult;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
 
 /**
  * Created by Tony Lee on 2016/3/2.
  * <p>
  * 阿里云后台控制实现类
  * <p>
  * 功能实现
  * 文件传输服务
  * <p>
  * 规划设计
  * 文件大小、进度展示
  * 历史文件
  * 垃圾箱功能
  * 根据ID进行索引 下载(需要OAS支持)
  * <p>
  * 前端控制台位置 https://oas.console.aliyun.com/console/index#/oas/
  */
 public class Console extends HttpServlet implements Configuration {
 
     @Test
     public void test() throws Exception {
 //      OASDescriptor OASResult OASRequest 低级接口
 
         // 1 向库里添加文件
 //        addToRemote(null, "src/OAS/Resources/upload/", "fileIndex_v2.json");
         // 2 更新本地索引文件
 //        updateIndex(new String[]{"[id]", "moocRar.rar", "rar", new Date().toString(), "Lee"});
         // 3 定期更新库里索引文件(待定)
 //        addToRemote(null, "src/OAS/Resources/upload/", "fileIndex_v2.json");
 
         // 列出近期JOB
 //        listJob(null, 100);
         // 测试 从库里下载文件
//         dlFromRemote("23DD22781D994424F36768DF6794810BA1586C053194449CB64FDDA79A4BF5699173A2BFC835221E8B1A09952E29E48178393F41CCAB778C05A37792AA50CF73",
//                 downloadFolder);
 
     }
 
     /***************** Controller *****************/
 
     /**
      * 采用get的通讯方式
      */
     public void doGet(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
 
     }
 
     /**
      * 采用post的通讯方式
      */
     public void doPost(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
         doGet(request, response);
     }
 
     /***************** 本地文件控制 *****************/
 
     /**
      * 创建、增加
      */
     public static File createLocal(File file, String path) throws Exception {
         File newFile = file == null ? FileOperation.getOrCreate(path) : file;
         return newFile;
     }
 
     /**
      * 删除、移出
      */
     public static void deleteLocal(String path) throws Exception {
         File file = new File(path);
         FileOperation.deleteFile(path);
     }
     /** 修改 */
 
     /** 查询 */

     /***************** 远程文件控制 *****************/

     /**
      * 远程登录
      *
      * @param request
      * @param response
      * @throws IOException
      */
     public static void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
         new VaultOperation().login(request, response);
     }
 
     /**
      * 更新索引文件
      *
      * @param dataSetTitle 数据集标题
      * @param params 数据
      * @throws Exception
      */
     public static void updateIndex(String dataSetTitle, String[] params) throws Exception {
         JSONConverter.addFromBottom(dataSetTitle, JSONConverter.buildJson(params));
     }
 
     /**
      * 增加文件
      */
     public static String[] addToRemote(HttpServletRequest request, File file, String path, String name) throws Exception {
         // 上传文件
         File newFile = createLocal(file, (path + name));
         VaultOperation vaultOperation = new VaultOperation();
         UploadResult uploadResult = null;
         String[] oasInfo = null;
         if(newFile.length() < 1024L * 1024L * 100) {
             uploadResult = vaultOperation.archiveManager(request, newFile);
         } else {
             uploadResult = vaultOperation.multipartUpload(request, newFile);
         }
         if(uploadResult != null)
         // 返回文件信息
 //        String[] fileInfo = FileOperation.getFileInfo(newFile);
         oasInfo = new String[]{
                 uploadResult.getArchiveId(), "", ""
         };
         return oasInfo;
     }
 
     /**
      * 下载文件
      */
     public static void dlFromRemote(HttpServletRequest request, String id, String path) throws Exception {
         new VaultOperation().download(request, id, path);
     }
 
     /** 删除文件 */
 
     /** 修改文件 */
 
     /**
      * 查询Job列表
      */
     public static void listJob(HttpServletRequest request, String vaultId, int limit) throws Exception {
         new VaultOperation().lookHistory(request, vaultId, limit);
     }
 
     /**
      * 查询文件列表
      *
      * @param params
      * @throws Exception
      */
     public static void listFiles(String[] params) throws Exception {
         // 通过索引文件实现
     }
 
 
 }

