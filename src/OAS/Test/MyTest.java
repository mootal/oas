package OAS.Test;

import OAS.Infrastructure.Configuration;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;

/**
 * Created by Mono on 2016/3/13.
 */
public class MyTest {

    @Test
    public void test() {

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(
                    Configuration.class.getResource("oasUser.properties").getPath()
            ));
            String AccessKeyId = prop.getProperty("AccessKeyId");
            String AccessKeySecret = prop.getProperty("AccessKeySecret");
            String VaultName = prop.getProperty("VaultName");
            System.out.println(AccessKeyId);
        } catch(IOException e) {
            e.printStackTrace();
        }


//        for(int i=1; i<=20;i++){
//            System.out.print(rounding(balance1(1.2, i)) + "  | ");
//        }
//        System.out.println();
//        for(int i=1; i<=20;i++){
//            System.out.print(rounding(balance2(3.5, 0.14, i)) + " | ");
//        }
    }

    /**
     * 参数
     *  初始投资额
     *  投资回报 利润率或增长率
     *  投资年限
     *
     *  案例1
     *  投入3.5W 除去本金 每年收回1.2W
     *
     *  案例2
     *  投入3.5W 包括本金 每年增值15%
     *
     *  分别求得每年末实际余额 每年度利润率
     */
    /**
     * 案例1
     * @param income 每期回报额
     * @param year   期数
     * @return
     */
    public static double balance1(double income, int year) {
        return income * year;
    }

    /**
     * 案例2
     * @param principal 本金
     * @param interest  平均每期利润率
     * @param year      期数
     * @return
     */
    public static double balance2(double principal, double interest, int year) {

        return principal * getPower((1 + interest), year);
    }

    /**
     * 收益率变动衡量
     * 收益率 = 净利润 / 本金
     *
     */

    public static void profit1() {

    }

    public static void profit2() {
        // 固定值
    }


    /*******************工具*******************/


    /**
     * 四舍五入
     * @param principal 基数
     * @return
     */
    private static double rounding(double principal) {
        BigDecimal b = new BigDecimal(principal);
        return b.setScale(2, RoundingMode.HALF_EVEN).doubleValue();
    }

    /**
     * 求幂
     * @param base
     * @param index
     * @return
     */
    public static double getPower(double base, double index) {
        if(base !=0 && index == 0) {
            return 1;
        }
        double power = 1;
        for(int i=0; i< index; i++) {
            power = base * power;
        }
        return power;
    }
}
