package OAS.Infrastructure;

/**
 * Created by Tony Lee on 2016/1/13.
 */
public interface Configuration {

    String DOWNLOAD = "download";
    String UPLOAD = "upload";
    String OASUPLOAD = "oasUpload";

    String FAIL = "error";
    String DONE = "success";
    String ALWAYS = "complete with problem";

    /************ 文件信息 Archive ************/
    // 静态文件相对地址 本地测试
    String uploadFolder = "src/OAS/Resources/upload/"; // 上传文件夹
    String downloadFolder = "src/OAS/Resources/download/"; // 下载文件夹

    String Test_dest = "src/OAS/Resources/";
    String Test_file = "src/OAS/test.txt";

    String ST_Grid2_SRC = "src/OAS/Resources/upload/gridJS_demo.json";
    String ST_INDEX_SRC = "src/OAS/Resources/upload/fileArchive_v2.json";
    String ST_INDEX_DST = "src/OAS/Resources/upload/fileIndex_v2.json";

    // 部署到服务器的相对地址
    String DY_Grid2_SRC = "../Resources/upload/gridJS_demo.json";
    String DY_INDEX_SRC = "../Resources/upload/fileArchive_v2.json";
    String DY_INDEX_DST = "../Resources/upload/fileIndex_v2.json";
    String DY_UP_Folder = "../Resources/upload/";
    String DY_DL_Folder = "../Resources/download/";

    // 绝对地址
    String SF_UP_Folder = "E:/Workspaces/OAS/oas/src/OAS/Resources/upload/"; // 上传文件夹
    String SF_DL_Folder = "E:/download/"; // 下载文件夹
    String SF_INDEX_SRC = "E:/Workspaces/OAS/oas/src/OAS/Resources/upload/fileArchive_v2.json";
    String SF_INDEX_DST = "E:/Workspaces/OAS/oas/src/OAS/Resources/upload/fileIndex_v2.json";

    String TechBook_fileId = "79D378CE0F82F082A34E61B509B4DF6857981D48795540B7B68F8B286B5F3CF12CD85527751744BECF8D31005BE1D83EE2F33E6035BBF17DB063A333A8C613DD";
    String Test_fileId = "FB85FA508FF5F4AFA66B2E8966243C7B771647673D8B498389C302CA42A595A26BF1296460A437F784289E969D22118FA280663B995CCB275EF36FA2ECB5A9F6";

    String Test_Bigfile = "src/OAS/big.txt";
    String Test_Destfile = "src/OAS/dest.txt";

    /************ 连接信息 ************/
    // session
    // cookie
    // 连接池
    // 连接超时
    // 半小时
    int cookieExpire = 1800;
    int sessionExpire = 1800;

    /************ 库信息 Vault ************/

    // 每个用户最多创建10个vault
    String publicAddr = "cn-hangzhou.oas.aliyuncs.com";
    String innerAddr = "cn-hangzhou.oas-internal.aliyuncs.com";

    String VaultName1 = "520";
    String vaultId1 = "";
    String publicAddr1 = "";
    String innerAddr1 = "";

    int PORT_80 = 80;
    String RegionName_HangZhou = "cn-hangzhou";
    String RegionName_ShenZhen = "cn-shenzhen";

    String PROTOCOL_HTTP = "http://";
    String PROTOCOL_HTTPS = "https://";
    String PROTOCOL_FTP = "ftp://";
    String PROTOCOL_WEBSOCKET = "ws://";

    String PUBLIC_DOMAIN = ".oas.aliyuncs.com";
    String PRIVATE_DOMAIN = ".oas-internal.aliyuncs.com";

}
