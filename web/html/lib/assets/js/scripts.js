
jQuery(document).ready(function() {

    $('.page-container form').submit(function(e){
        e.preventDefault();

        var username = $(this).find('.username').val();
        var password = $(this).find('.password').val();
        var repository = $(this).find('.repository').val();

        if(!username) {
            $(this).find('.error').fadeOut('fast', function(){
                $(this).css('top', '55px');
            });
            $(this).find('.error').fadeIn('fast', function(){
                $(this).parent().find('.username').focus();
            });
            return false;
        }
        if(!password) {
            $(this).find('.error').fadeOut('fast', function(){
                $(this).css('top', '125px');
            });
            $(this).find('.error').fadeIn('fast', function(){
                $(this).parent().find('.password').focus();
            });
            return false;
        }
        if(!repository) {
            $(this).find('.error').fadeOut('fast', function(){
                $(this).css('top', '195px');
            });
            $(this).find('.error').fadeIn('fast', function(){
                $(this).parent().find('.repository').focus();
            });
            return false;
        }

        login(username, password, repository);
    });

    $('.page-container form .username, .page-container form .password, .page-container form .repository').keyup(function(){
        $(this).parent().find('.error').fadeOut('fast');
    });

});
