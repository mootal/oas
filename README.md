项目说明
* 画眉文件系统 | Huamei File System | Huamei.io
* OAS子系统 数据归档存储

开发准备
* 去阿里云注册自己的OAS帐号和仓库

编码
* GBK: 资源文件 说明文件 HTML文件 
* UTF-8: 常规文件 
* git gui : git config --global gui.encoding gbk
* git commit : git config --global i18n.commitencoding utf-8
* git log : git config --global i18n.logoutputencoding utf-8

文件夹层次和命名规范
* src以下开始为1，2，3。。。层
* 第一层全大写
* 第二层首字母大写驼峰
* 第三层首字母小写驼峰

资源管理
* jar包是外部依赖的
* js框架 Bootstrap Datagrid http://www.pontikis.net/labs/bs_grid/demo/

部署 
* 不依赖maven 原始部署
* web目录 out目录 服务容器目录关系
    * exploded模式
1 设定输出文件夹为out
2 编译本地java文件后 生成production目录 拷贝class
3 启动tomcat时 以web > prod的寻源优先级 将class文件导入artifacts中的相关目录
4 此处注意的问题 如果web目录中有class文件 要注意其时效性 因为从不更新 本地都更新到production了
5 若要保证时效性 可以删除web目录中的class文件
    * war模式
1 上述的文件不再出现在设定的out文件夹 而是被部署到tomcat所在的文件夹里 
2 尤其是一些资源文件的位置
